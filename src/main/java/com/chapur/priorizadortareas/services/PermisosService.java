package com.chapur.priorizadortareas.services;

import com.chapur.priorizadortareas.model.Permiso;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PermisosService {

	@Transactional(readOnly = true)
    int tienePermisoAplicacion(int numiden, int idaplicacion);

	@Transactional(readOnly = true)
    List<Permiso> permisosUsuario(int idaplicacion, int idusuario);

	@Transactional(readOnly = true)
    void validaModuloPermiso(int idpermiso, int idmodulo, int idnumiden);
}