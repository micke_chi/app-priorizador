package com.chapur.priorizadortareas.services;

import com.chapur.priorizadortareas.model.Login;
import org.springframework.transaction.annotation.Transactional;

public interface LoginService {
	
	@Transactional(readOnly = true)
    Login acceso(String usuario, String password);
}