package com.chapur.priorizadortareas.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class HomeController {

	@Value("${version}")
	private String version;

	@RequestMapping("/")
	public ModelAndView index(HttpServletRequest request) {

		/*String url =  request.getRequestURL().toString();
		System.out.println("url base: " + url);

		ModelAndView mv = new ModelAndView("web/panel-incrementos");
		mv.addObject("urlb", url);
		mv.addObject("version", version);*/

		ModelAndView mv = new ModelAndView("web/panel-admin");

		System.out.println("Hola en el adin");

		return mv;
	}

	
	@GetMapping("/login")
	public String login(Principal principal) {

		System.out.println("Hola en el adin");
		if (principal == null) {
			return "login/login";
		}
		return "redirect:/";
	}
}