package com.chapur.priorizadortareas.configuration;

import com.chapur.priorizadortareas.exceptions.PermisosException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomLoginFailureHandler implements AuthenticationFailureHandler {

	@Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException {
        String message = "";
        if (exception.getClass() == UsernameNotFoundException.class) {
            message = "1";
        } else if (exception.getClass() == BadCredentialsException.class) {
            message = "2";
        } else if (exception.getClass() == PermisosException.class) {
            message = "3";
        }
        response.sendRedirect(String.format("login?error=%s", message));
    }
}