package com.chapur.priorizadortareas.configuration;

import com.chapur.priorizadortareas.constants.Mensaje;
import com.chapur.priorizadortareas.constants.Permisos;
import com.chapur.priorizadortareas.exceptions.PermisosException;
import com.chapur.priorizadortareas.model.Login;
import com.chapur.priorizadortareas.model.Permiso;
import com.chapur.priorizadortareas.services.LoginService;
import com.chapur.priorizadortareas.services.PermisosService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	private LoginService loginService;

    private PermisosService permisosService;

    private boolean isDev;

    CustomAuthenticationProvider(LoginService loginService, PermisosService permisosService) {
        this.loginService = loginService;
        this.permisosService = permisosService;
        this.isDev = false;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws BadCredentialsException {
        if (isDev) {
            Login devLogin = new Login();
            devLogin.setNombre("Miguel Chi");
            devLogin.setNumiden(341362);
            devLogin.setNomina(22222);
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            grantedAuths.add(new SimpleGrantedAuthority("755"));
            grantedAuths.add(new SimpleGrantedAuthority("756"));
            grantedAuths.add(new SimpleGrantedAuthority("757"));
            return new CustomAuthenticationToken(devLogin, "pass", 1, grantedAuths);
        }

        String usuario = authentication.getName();
        String password = authentication.getCredentials().toString();
        Login login = loginService.acceso(usuario, password);

        //Usuario valido
        if (login.getExito() == Mensaje.EXITO) {
            //Permisos en la aplicación
            if (permisosService.tienePermisoAplicacion(login.getNumiden(), Permisos.IDAPLICACION) == Permisos.TIENE_PERMISO) {
                //Asigna permisos al usuario
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                for (Permiso p : permisosService.permisosUsuario(Permisos.IDAPLICACION, login.getNumiden())) {
                    grantedAuths.add(new SimpleGrantedAuthority("" + p.getIdpermisos()));
                }
                return new CustomAuthenticationToken(login, password, login.getNumiden(), grantedAuths);
            } else {
                throw new PermisosException("Usuario no tiene permisos en la aplicación");
            }
        } else {
            throw new BadCredentialsException("Usuario o contraseña incorrectos");
        }
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
}