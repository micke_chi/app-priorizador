package com.chapur.priorizadortareas.configuration;

import com.chapur.priorizadortareas.services.LoginService;
import com.chapur.priorizadortareas.services.PermisosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private LoginService loginService;

    private PermisosService permisosService;

    @Autowired
    private void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    @Autowired
    private void setPermisoService(PermisosService permisosService) {
        this.permisosService = permisosService;
    }

    @Override
    @Autowired
    public void configure(AuthenticationManagerBuilder auth) {
        CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider(loginService, permisosService);
        auth.authenticationProvider(authProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/js/*.js").permitAll()
                .antMatchers("/css/*.css").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/login*").anonymous()
                .anyRequest().authenticated()

                .and()
                .formLogin()
                .loginPage("/login")
                .failureHandler(new CustomLoginFailureHandler())
                .defaultSuccessUrl("/", true)
                .permitAll()

                .and()
                .logout()
                .permitAll();
    }
}