package com.chapur.priorizadortareas.configuration;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;

public class CustomAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 1L;

    private final int numiden;

    public CustomAuthenticationToken(Object principal, Object credentials, int numiden) {
        super(principal, credentials);
        this.numiden = numiden;
    }

    public CustomAuthenticationToken(Object principal, Object credentials, int numiden, GrantedAuthority[] authorities) {
        super(principal, credentials, Arrays.asList(authorities));
        this.numiden = numiden;
    }

    public CustomAuthenticationToken(Object principal, Object credentials, int numiden, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
        this.numiden = numiden;
    }

    public int getNumiden() {
        return numiden;
    }
}