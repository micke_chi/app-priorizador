package com.chapur.priorizadortareas.dao.ventas;

import com.chapur.priorizadortareas.model.Login;
import com.chapur.priorizadortareas.services.LoginService;
import com.chapur.priorizadortareas.utils.Seguridad;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.List;

@Service
public class LoginDAO implements LoginService {

    @PersistenceContext(unitName = "source")
    private EntityManager manager;

    @SuppressWarnings("unchecked")
    @Override
    public Login acceso(String usuario, String password) {
        Login login = new Login();
        try {
            StoredProcedureQuery query = manager.createStoredProcedureQuery("VENTAS.ECOM_LOGUEO");
            query.registerStoredProcedureParameter("P_USUARIO", String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("P_CONTRASENIA", String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("OUT_RESULTSET", void.class, ParameterMode.REF_CURSOR);
            query.setParameter("P_USUARIO", usuario);
            query.setParameter("P_CONTRASENIA", Seguridad.Chapur.encriptar(password));
            query.execute();

            List<LoginService> list = (List<LoginService>) query.getResultList();
            for (Object aList : list) {
                Object[] obj = (Object[]) aList;
                login = new Login(
                        Integer.valueOf(String.valueOf(obj[0])),
                        Integer.valueOf(String.valueOf(obj[1])),
                        String.valueOf(obj[2]),
                        Integer.valueOf(String.valueOf(obj[3])),
                        String.valueOf(obj[4])
                );
            }
            manager.close();
        } catch (PersistenceException pe) {
            System.out.println("Error en LoginServiceDao {acceso}: " + pe.getMessage());
        }
        return login;
    }
}