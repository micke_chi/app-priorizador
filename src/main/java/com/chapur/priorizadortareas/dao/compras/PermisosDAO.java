package com.chapur.priorizadortareas.dao.compras;

import com.chapur.priorizadortareas.model.Permiso;
import com.chapur.priorizadortareas.services.PermisosService;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class PermisosDAO implements PermisosService {

    @PersistenceContext(unitName = "compras")
    private EntityManager manager;

    @Override
    public int tienePermisoAplicacion(int numiden, int idaplicacion) {
        int tienePermiso = 0;
        try {
            StoredProcedureQuery query = manager.createStoredProcedureQuery("COMPRAS.ECOM_TIENE_PERMISOS");
            query.registerStoredProcedureParameter("P_NUMIDEN", int.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("P_APLICACION", int.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("OUT_RESULTSET", int.class, ParameterMode.OUT);
            query.setParameter("P_NUMIDEN", numiden);
            query.setParameter("P_APLICACION", idaplicacion);
            query.execute();
            manager.close();
            tienePermiso = (int) query.getOutputParameterValue("OUT_RESULTSET");
        } catch (PersistenceException pe) {
            System.out.println("Error en PermisosServiceDao: " + pe.getMessage());
        }
        return tienePermiso;
    }

    @Override
    public List<Permiso> permisosUsuario(int idaplicacion, int idusuario) {
        List<Permiso> permisos = new ArrayList<>();
        StoredProcedureQuery query = manager.createStoredProcedureQuery("COMPRAS.PERMISOS_USUARIO");
        query.registerStoredProcedureParameter("IDAPLICACION", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("IDUSUARIO", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("OUTRESULTSET", void.class, ParameterMode.REF_CURSOR);
        query.setParameter("IDAPLICACION", idaplicacion);
        query.setParameter("IDUSUARIO", idusuario);
        query.execute();
        
        for (Object aList : query.getResultList()) {
            Permiso p = new Permiso(idusuario, idaplicacion, Long.parseLong(String.valueOf(aList)));
            permisos.add(p);
        }
        manager.close();
        return permisos;
    }
    
    /**
     * No usado
     */
    @Override
    public void validaModuloPermiso(int idpermiso, int idmodulo, int idnumiden) {
        StoredProcedureQuery query = manager.createStoredProcedureQuery("COMPRAS.ECOM_VALIDPER_APLICACION");
        query.registerStoredProcedureParameter("P_PERMISO", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("P_MODULO", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("P_NUMIDEN", int.class, ParameterMode.IN);
        query.registerStoredProcedureParameter("OUT_RESULTSET", void.class, ParameterMode.REF_CURSOR);
        query.setParameter("P_PERMISO", idpermiso);
        query.setParameter("P_MODULO", idmodulo);
        query.setParameter("P_NUMIDEN", idnumiden);
        query.execute();

        //List<PermisosService> list = (List<PermisosService>) query.getResultList();
        manager.close();
    }
}