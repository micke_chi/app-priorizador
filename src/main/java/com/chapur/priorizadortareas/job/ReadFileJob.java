package com.chapur.priorizadortareas.job;


import com.chapur.priorizadortareas.batch.PersonProcessor;
import com.chapur.priorizadortareas.batch.PersonStreamWriter;
import com.chapur.priorizadortareas.batch.ToUpperCaseJobExecutionListener;
import com.chapur.priorizadortareas.model.Person;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * @author furkand
 * 10/26/2018 5:57 PM
 */
@Configuration
@EnableBatchProcessing
@Component
public class ReadFileJob {





    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private Step toUpperCaseConverter;

    @Bean
    public FlatFileItemReader<Person> reader() {
        return new FlatFileItemReaderBuilder<Person>()
                .name("personReader")
                .resource(new ClassPathResource("data.csv"))
                .delimited()
                .names(new String[]{"name", "surname"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                    setTargetType(Person.class);
                }})
                .build();
    }

    @Bean
    public PersonProcessor processor() {
        return new PersonProcessor();
    }

    @Bean
    public PersonStreamWriter writer() {
        return new PersonStreamWriter();
    }

    @Bean
    public ToUpperCaseJobExecutionListener listener() {
        return new ToUpperCaseJobExecutionListener();
    }

    @Bean("toUpperCaseJob")
    public Job convertToUpperCaseJob(ToUpperCaseJobExecutionListener listener) {
        return this.jobBuilderFactory.get("toUpperCaseJob")
                .listener(listener)
                .flow(this.toUpperCaseConverter)
                .end()
                .build();
    }

    @Bean
    public Step toUpperCaseConverter() {
        return this.stepBuilderFactory.get("toUpperCase")
                .<Person, Person>chunk(10)
                .reader(this.reader())
                .processor(this.processor())
                .writer(this.writer())
                .build();
    }

}
