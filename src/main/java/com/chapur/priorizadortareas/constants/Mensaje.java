package com.chapur.priorizadortareas.constants;

public final class Mensaje {

    public static final int ERROR = 0;

    public static final int EXITO = 1;
}
