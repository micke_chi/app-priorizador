package com.chapur.priorizadortareas.constants;

public final class Permisos {

    /**
     * ID Apliación
     */
    public static final int IDAPLICACION = 55;

    /**
     * Módulos
     */
    //public static final int ENVIO = 0;
    public static final int CREACION = 453;
    public static final int EDICION = 454;

    /**
     * Permisos
     */
    //public static final String GUARDAR = "0";
    public static final String CREAR = "755";
    public static final String SURBIR_ARCHIVO = "757";
    public static final String EDITAR = "756";

    /**
     * Mensajes respuesta
     */
    public static final int TIENE_PERMISO = 1;
    public static final int NO_TIENE_PERMISO = 0;
}