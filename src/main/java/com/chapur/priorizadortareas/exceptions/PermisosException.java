package com.chapur.priorizadortareas.exceptions;

import org.springframework.security.core.AuthenticationException;

@SuppressWarnings("serial")
public class PermisosException extends AuthenticationException {

    public PermisosException(String msg, Throwable t) {
        super(msg, t);
    }

    public PermisosException(String msg) {
        super(msg);
    }
}
