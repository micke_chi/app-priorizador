package com.chapur.priorizadortareas.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

public class Login {
	
    private Integer numiden;

    private Integer nomina;

    private String nombre;

    private Integer exito;

    private String mensaje;

    public Login() {
    }

    public Login(Integer numiden, Integer nomina, String nombre, Integer exito, String mensaje) {
        this.numiden = numiden;
        this.nomina = nomina;
        this.nombre = nombre;
        this.exito = exito;
        this.mensaje = mensaje;
    }

    public Integer getNumiden() {
        return numiden;
    }

    public void setNumiden(Integer numiden) {
        this.numiden = numiden;
    }

    public Integer getNomina() {
        return nomina;
    }

    public void setNomina(Integer nomina) {
        this.nomina = nomina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getExito() {
        return exito;
    }

    public void setExito(Integer exito) {
        this.exito = exito;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return nombre + " (" + this.nomina + ")";
    }

    @SuppressWarnings("unchecked")
	public static boolean hasAuthority(String aut){
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasAuthority = false;
        for (GrantedAuthority authority : authorities) {
            hasAuthority = authority.getAuthority().equals(aut);
            if (hasAuthority) {
                return true;
            }
        }
        return false;
    }
}