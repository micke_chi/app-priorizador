package com.chapur.priorizadortareas.model;

public class Permiso {
	
    private Integer numiden;

    private Integer idaplicacion;

    private Long idpermisos;

    public Permiso() {
    }

    public Permiso(Integer numiden, Integer idaplicacion, Long idpermisos) {
        this.numiden = numiden;
        this.idaplicacion = idaplicacion;
        this.idpermisos = idpermisos;
    }

    public int getNumiden() {
        return numiden;
    }

    public void setNumiden(Integer numiden) {
        this.numiden = numiden;
    }

    public int getIdaplicacion() {
        return idaplicacion;
    }

    public void setIdaplicacion(Integer idaplicacion) {
        this.idaplicacion = idaplicacion;
    }

    public Long getIdpermisos() {
        return idpermisos;
    }

    public void setIdpermisos(Long idpermisos) {
        this.idpermisos = idpermisos;
    }
}
