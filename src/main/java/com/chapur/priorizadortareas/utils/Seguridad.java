package com.chapur.priorizadortareas.utils;

public class Seguridad {

    private String clave;
    private String alfabeto;

    //------------------------------------------------------------------------------
    //DAPC  23/11/2010 Implementacion de los Metodos para la Encriptacion
    //------------------------------------------------------------------------------
    public static final Seguridad Chapur = new Seguridad("CHAPUR_ENCRIPTACION");

    private Seguridad(String _clave) {
        clave = _clave;
        alfabeto = "!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
        //  alfabeto =	"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890@<>{}[]-+?��!";
    }

    //------------------------------------------------------------------------------
    public String encriptar(String _source) {

        String stCadenaEncriptada = "";
        int i, movimiento, rotacion, total;
        int pos, pos2;
        char[] sour = _source.toCharArray();
        char[] cve = clave.toCharArray();
        char[] alf = alfabeto.toCharArray();
        for (i = 0, rotacion = 0; i < _source.length(); i++, rotacion++) {

            if (rotacion == (clave.length()))
                rotacion = 0;
            //Si se acaba la semilla, regresar al principio

            pos = alfabeto.indexOf(sour[i]);
            pos2 = alfabeto.indexOf(cve[rotacion]);

            total = (int) ((pos + pos2) % (alfabeto.length()));
            //El total que marca la posicion de la letra y la semilla sumadas

            if ((total) > (alfabeto.length()))
                movimiento = total - alfabeto.length();
            else
                movimiento = total;
            //Para asegurarnos que esta dentro del rango

            stCadenaEncriptada += alf[movimiento];

        }// FIN : for(i=0,rotacion=0;i<texto.length();i++,rotacion++)
        return stCadenaEncriptada;
    }

    //------------------------------------------------------------------------------
    public String desEncriptar(String _source) {

        String stCadenaEncriptada = "";
        int i, movimiento, rotacion, total;
        int pos, pos2;
        char[] sour = _source.toCharArray();
        char[] cve = clave.toCharArray();
        char[] alf = alfabeto.toCharArray();
        for (i = 0, rotacion = 0; i < _source.length(); i++, rotacion++) {

            if (rotacion == (clave.length()))
                rotacion = 0;

            pos = alfabeto.indexOf(sour[i]);
            pos2 = alfabeto.indexOf(cve[rotacion]);

            total = (int) ((pos - pos2)) % (alfabeto.length());
            if ((total) < 0)
                movimiento = total + alfabeto.length();
            else
                movimiento = total;

            stCadenaEncriptada += alf[movimiento];
        }
        return stCadenaEncriptada;
    }
}