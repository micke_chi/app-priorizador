package com.chapur.priorizadortareas;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@EnableAsync
@SpringBootApplication
public class PriorizadortareasApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(PriorizadortareasApplication.class, args);
	}

}
